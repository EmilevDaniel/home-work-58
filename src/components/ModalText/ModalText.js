import React from 'react';
import Button from "../UI/Button/Button";

const ModalText = props => {
    return (
        <>
            <h3>{props.title}
                <span onClick={props.onCancel} style={{float: 'right', cursor: 'pointer'}}>X</span>
            </h3>
            <p>bla bla</p>
            <p>
                <Button onClick={props.onCancel} type="Danger">Cancel</Button>
                <Button onClick={props.onContinue} type="Success">Continue</Button>
            </p>
        </>
    );
};

export default ModalText;