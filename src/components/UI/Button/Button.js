import React from 'react';
import './Button.css'

const Button = ({children, type, onClick}) => (
    <button onClick={onClick}
            className={['Button', type].join(' ')}
    >
        {children}
    </button>
);

export default Button;