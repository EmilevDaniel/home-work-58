import React from 'react';
import './Alert.css'
import {useState} from "react";

const Alert = (props) => {

    const [close, setClose] = useState(true);
    
    const closeAlert = () => {
        if (props.dismiss === true) {
            setClose(false)
        }
    };

    return (
        close ? <div className={['Alert', props.type].join(' ')}
        >
            {props.children}
            {props.dismiss ? <span onClick={closeAlert} style={{float: 'right', cursor: 'pointer'}}>X</span> : null}
        </div> : null

    );
};

export default Alert;