import React from 'react';
import './BackDrop.css'

const BackDrop = ({show, onClick}) => (
    show ? <div onClick={onClick} className="Backdrop"/> : null
);
export default BackDrop;