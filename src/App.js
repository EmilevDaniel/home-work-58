import './App.css';
import Modal from "./components/UI/Modal/Modal";
import {useState} from "react";
import ModalText from "./components/ModalText/ModalText";
import Alert from "./components/UI/Alert/Alert";

function App() {
    const [purchasing, setPurchasing] = useState(false);

    const cancelModalHandler = () => {
        setPurchasing(false)
    };

    const continueModalHandler = () => {
        alert('You pressed continue');
    };

    const modalHandler = () => {
        setPurchasing(true);
    };

    return (
        <div className="App">
            <Modal show={purchasing} close={cancelModalHandler}>
                <ModalText
                    onCancel={cancelModalHandler}
                    onContinue={continueModalHandler}
                    title={"My title"}
                />
            </Modal>
            <button onClick={modalHandler}>START</button>
            <Alert dismiss={true} type="Success">Bla BLa BLa </Alert>
            <Alert dismiss={true} type="Danger">Bla BLa BLa </Alert>
            <Alert type="Primary">Bla BLa BLa </Alert>
            <Alert type="Secondary">Bla BLa BLa </Alert>
        </div>
    );
}

export default App;
